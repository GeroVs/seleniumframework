package utilities;

import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class DriverBuilder {

	private DriverType driverType;
	private String[] defaultOptions;
	
	
	
	public DriverBuilder(DriverType driverType) {
		this.driverType = driverType;
	}
	
	public DriverBuilder withDefaultOptions(){
        
        switch (driverType){
            case IE:
                defaultOptions = new String[]{"ignoreZoomSettings", "ignoreProtectedModeSettings", "requireWindowFocus"};
                break;
            case Chrome:
                defaultOptions = new String[] {"disable-extensions", "--disable-notifications", "--start-maximized"};
                break;

            case FireFox:
                defaultOptions = new String[] {"marionette"};
                break;
        }
        
        return this;
    }
	
	
	public WebDriver build() {
		
		WebDriver driver = null;
		
		switch (driverType) {
		case Chrome:
			System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\chromedriver.exe");
			ChromeOptions chromeOptions = (ChromeOptions)getOptions(defaultOptions);
			driver = new ChromeDriver(chromeOptions);			
			break;

		case FireFox:
			break;
			
			
		default:
			System.out.println("Wrong driver type option");
			break;
		}
		
		
		//driver.manage().window().maximize();
		
		return driver;
	}
	
	
	private Object getOptions(String ...opts) {
		
		switch (driverType) {
		case Chrome:
			ChromeOptions options = new ChromeOptions();
			return options.addArguments(opts);
			

		default:
			break;
		}
		
		return null;
	}
}
