package utilities;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class DriverHandler {

	static JavascriptExecutor js;
	
	public void PerformAction(WebDriver driver, SeleniumActions action, WebElement element, String value) {

		js = ((JavascriptExecutor)driver);
		
		switch (action) {

		case Navigate:
			Navigate(driver, value);
			break;

		case Click:
			Click(element);
			break;

		case Type:
			Type(element, value);
			break;

        case AltType:
			AltType(element, value);
			break;

		default:
			break;
		}

	}


	private void Navigate(WebDriver driver, String value) {

		try {
			String url;
			if(!value.contains("http://") || !value.contains("https://")) {
				url = "http://" + value; 
			}else
				driver.navigate().to(value);


		}catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	private void Click(WebElement element) {

		try {
			
			Highlight(element);
			element.click();

		}catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}


	private void Type(WebElement element, String value) {

		try {
			
			Highlight(element);
			element.sendKeys(value);

		}catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}
	
	private void AltType(WebElement element, String value) {

		try {
			
			Highlight(element);
			element.click();
			element.sendKeys(value);

		}catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}
	

	public static void Highlight(WebElement element) {
		String border;
		border = "#f00 solid 5px";
		js.executeScript("arguments[0].style.outline = '" + border + "'; ", element);

	}



}
