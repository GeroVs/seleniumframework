package testCases;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import PageObject.GoogleWebsite.GoogleHomePage;
import utilities.DriverBuilder;
import utilities.DriverHandler;
import utilities.DriverType;
import utilities.SeleniumActions;

public class GoogleSearchTest extends DriverHandler {
	
	WebDriver driver;
	
	public GoogleSearchTest(DriverType driverType) {
		
		driver = new DriverBuilder(driverType)
				.withDefaultOptions()
				.build();
		
		
		driver.navigate().to("http://www.google.com");

		PerformAction(driver,SeleniumActions.Type, GoogleHomePage.Google_TextBox(driver), "Hexaware");
		
		GoogleHomePage.Google_TextBox(driver).sendKeys(Keys.ENTER);
	
		
		
	}
}
